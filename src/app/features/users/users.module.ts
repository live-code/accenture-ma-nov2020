import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { UserFormComponent } from './home/components/user-form.component';
import { UserListComponent } from './home/components/user-list.component';
import { UserListItemComponent } from './home/components/user-list-item.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UsersRoutingModule } from './users-routing.module';
import { UsersGuideComponent } from './guide/users-guide.component';
import { UsersGroupsComponent } from './groups/users-groups.component';
import { UsersHomeComponent } from './home/users-home.component';

@NgModule({
  declarations: [
    // home
    UsersHomeComponent,
    UsersComponent,
    UserFormComponent,
    UserListComponent,
    UserListItemComponent,
    // guide
    UsersGuideComponent,
    // groups
    UsersGroupsComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    UsersRoutingModule
  ]
})
export class UsersModule {}

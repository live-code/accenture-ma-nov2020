import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../../model/user';

@Component({
  selector: 'app-user-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-user-list-item
      *ngFor="let user of users"
      [user]="user"
      [selected]="user.id === activeUser?.id"
      (setActiveUser)="setActiveUser.emit($event)"
      (deleteUser)="deleteUser.emit($event)"
    ></app-user-list-item>
  `,
})
export class UserListComponent {
  @Input() users: User[];
  @Input() activeUser: User;
  @Output() setActiveUser: EventEmitter<User> = new EventEmitter<User>();
  @Output() deleteUser: EventEmitter<number> = new EventEmitter<any>();
}

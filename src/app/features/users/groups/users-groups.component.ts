import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users-groups',
  template: `
    <button routerLink="../home">go to users page</button>
  `,
  styles: [
  ]
})
export class UsersGroupsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

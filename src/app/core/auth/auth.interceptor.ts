import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloneReq = req;
    if (this.authService.isLogged()) {
      cloneReq = req.clone({
        setHeaders: { Authorization: this.authService.token}
      });
    }
    return next.handle(cloneReq)
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 401:
              case 404:
                // .... Toast.notification('errore chiamata xyz')
                break;
              default:
                this.router.navigateByUrl('login');
            }
          }
          return throwError(err)
        })
      )
  }
}

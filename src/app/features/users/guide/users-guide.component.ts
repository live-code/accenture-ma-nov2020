import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users-guide',
  template: `
    <p>
      users-guide works!
    </p>
    <button routerLink="../home">go to users page</button>

  `,
  styles: [
  ]
})
export class UsersGuideComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

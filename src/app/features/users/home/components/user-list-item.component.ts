import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../../model/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li
      (click)="setActiveUser.emit(user)"
      class="list-group-item"
      [ngClass]="{'active': selected}"
    >
      {{user.name}} {{user.id}} {{user.phone}}
      <i class="fa fa-trash"
         (click)="deleteHandler(user.id, $event)"></i>
      
      <i class="fa fa-link" (click)="navigateTo(user.id, $event)"></i>
    </li>
  `,
})
export class UserListItemComponent {
  @Input() user: User;
  @Input() selected: boolean;
  @Output() setActiveUser: EventEmitter<User> = new EventEmitter<User>();
  @Output() deleteUser: EventEmitter<number> = new EventEmitter<any>();

  constructor(private router: Router) {
  }

  deleteHandler(id: number, event: MouseEvent): void {
    event.stopPropagation();
    this.deleteUser.emit(id);
  }

  navigateTo(id: number, event: MouseEvent): void {
    event.stopPropagation();
    this.router.navigateByUrl('/users-details/' + id)
  }
}

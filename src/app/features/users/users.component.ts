import { Component } from '@angular/core';

@Component({
  selector: 'app-users',
  template: `
    
    <button routerLink="home">go to home</button>
    <button routerLink="guide">go to guide</button>
    <button routerLink="groups">go to groups</button>
    
    <router-outlet></router-outlet>
  `,
})
export class UsersComponent {
}


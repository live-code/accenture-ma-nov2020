import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="container mt-3">
      <app-panel 
        title="STEP 1" 
        [open]="step1Opened"
      >
        <form #f="ngForm" (submit)="step1Opened = false; step2Opened = true">
          <br>
          <small *ngIf="userNameInput.errors?.required">required</small>
          <small *ngIf="userNameInput.errors?.minlength">min length 3 chars</small>
          <br>
          <input type="text" ngModel #userNameInput="ngModel"
                 [style.backgroundColor]="userNameInput.valid ? 'green' : 'red'"
                 [style.color]="userNameInput.valid ? 'white' : 'black'"
                 name="username" required minlength="3">
          <input type="text" ngModel name="password" required>
          <button [disabled]="f.invalid" type="submit">save</button>
        </form>
      </app-panel>

      <app-panel title="STEP 2" [open]="step2Opened">
        <input type="text">
        <input type="text">
        <input type="text">
      </app-panel>
    </div>
  `,
})
export class AppComponent {
  step1Opened = true;
  step2Opened = false;

  doSomething(): void {
    alert('do something')
  }

  save(): void {
    console.log('subtmit')
  }
}

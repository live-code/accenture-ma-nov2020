import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { AuthService } from '../../core/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  template: `
    <div class="alert alert-danger" *ngIf="authService.err">
      errore
    </div>
    
    {{authService.token}}
    <form [formGroup]="form" (submit)="loginHandler()">
      <input type="text" formControlName="username">
      <input type="text" formControlName="password">
      <button type="submit" [disabled]="form.invalid" >SAVE</button>
    </form>
    
    <hr>
   <!-- <form [formGroup]="formSearch">
      <input type="text" formControlName="city">
    </form>-->
  `,
})
export class LoginComponent {
  form: FormGroup;
  formSearch: FormGroup;

  constructor(
    private fb: FormBuilder,
    public authService: AuthService,
    private router: Router
  ) {
    this.form = fb.group({
      username: ['guest', Validators.compose([Validators.required])],
      password: 'yo!',
    });

    /*this.formSearch = fb.group({
      city: 'Rome',
    });

    this.formSearch.get('city').valueChanges
      .pipe(
        filter(text => text.length > 2),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(val => {
        console.log(val)
      })
*/
  }

   loginHandler(): void {
    const { username, password } = this.form.value;
    this.authService.login(username, password )
      .subscribe(res => {
        this.router.navigateByUrl('users');
      })
  }
}

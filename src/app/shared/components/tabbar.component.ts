import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-tabbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ul class="nav nav-tabs">
      <li 
        class="nav-item"
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a class="nav-link" [ngClass]="{'active': item.id === active?.id}">
          {{item[labelField]}}
        </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent {
  @Input() items: any[];
  @Output() tabClick: EventEmitter<any> = new EventEmitter<any>();
  @Input() active: any;
  @Input() labelField = 'label';

}

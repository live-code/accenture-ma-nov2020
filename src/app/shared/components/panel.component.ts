import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-panel',
  template: `
    <div class="card">
      <div class="card-header" (click)="headerClick.emit()">
        {{title}} 
        <i 
          *ngIf="icon" 
          class="pull-right" 
          [ngClass]="icon"
          (click)="iconClickHandler($event)"
        ></i>
      </div>
      <div class="card-body" *ngIf="open">
       <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class PanelComponent {
  @Input() title: string;
  @Input() icon: string;
  @Output() iconClick: EventEmitter<void> = new EventEmitter();
  @Output() headerClick: EventEmitter<void> = new EventEmitter();
  @Input() open = true;

  iconClickHandler(event: MouseEvent): void {
    event.stopPropagation();
    this.iconClick.emit();
  }
}

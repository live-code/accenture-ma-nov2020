import { Component } from '@angular/core';
import { City, Country } from '../../model/country';

@Component({
  selector: 'app-uikit',
  template: `
    <h3>Directives</h3>
     
    <span [appBg]="color" appUrl="http://www.fabiobiondi.io">fabiobiondi.io</span>
    <button appUrl="http://www.fabiobiondi.io">Visit my site</button>
    <button (click)="color = 'blue'" appIfLogged>blue</button>
    <button (click)="color = 'green'" *appIfSignIn>green</button>

    
    <h3>Components</h3>
    <div class="container mt-3">
      <app-tabbar 
        [items]="countries"
        [active]="activeCountry"
        (tabClick)="countryClickHandler($event)"
      ></app-tabbar>
      <app-tabbar
        [items]="activeCountry.cities"
        [active]="activeCity"
        labelField="name"
        (tabClick)="cityClickHandler($event)"
      ></app-tabbar>
    </div>
    
    <app-panel [title]="activeCity.name" *ngIf="activeCity">
      <app-static-map [city]="activeCity.name"></app-static-map>
    </app-panel>
    
    
  `,
})
export class DemoComponent {
  color = 'red';
  countries: Country[] = [
    {
      id: 1, label: 'italy',
      description: '',
      cities: [
        { id: 1001, name: 'Rome'},
        { id: 1002, name: 'Milan'},
        { id: 1003, name: 'Naples'},
      ]
    },
    {
      id: 2, label: 'Spain', description: 'bla bla 2',
      cities: [
        { id: 1005, name: 'Madrid'},
      ]
    },
    {
      id: 3, label: 'Germany', description: 'bla bla 3',
      cities: [
        { id: 1006, name: 'Berlin'},
        { id: 1007, name: 'Monaco'},
      ]
    },
  ];
  activeCountry: Country;
  activeCity: City;

  constructor() {
    this.countryClickHandler( this.countries[0] )
  }
  countryClickHandler(country: Country): void {
    this.activeCountry = country;
    if (this.activeCountry.cities.length) {
      this.activeCity = this.activeCountry.cities[0];
    }
  }

  cityClickHandler(city: City): void {
    this.activeCity = city;
    // window.open('https://it.wikipedia.org/wiki/' + country.label)
  }

}


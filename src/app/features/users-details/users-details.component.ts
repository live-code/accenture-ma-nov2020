import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { ActivatedRoute, Router } from '@angular/router';
import { retry } from 'rxjs/operators';

const API = 'http://localhost:3000';

@Component({
  selector: 'app-users-details',
  template: `
    
    <div class="alert alert-danger" *ngIf="error" (click)="error = false"> Errore grave! </div>
    <h1>{{user?.name}}</h1>
    <div *ngIf="!user">No data for this user</div>
    
    
    <button (click)="gotoNextUser()">NEXT</button>
  `,
})
export class UsersDetailsComponent implements OnInit {
  user: User;
  error = false;

  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private router: Router) {
    console.log(activatedRoute)
    this.activatedRoute.params
        .subscribe(params => {
          this.load(+params.id);
        });
  }

  load(id: number): void {
    this.http.get<User>(`${API}/users/${id}`)
      .subscribe(
        res => {
          this.error = false;
          this.user = res;
        },
        err => {
          this.error = true;
          this.user = null;
        }
      );
  }

  ngOnInit(): void {
  }

  gotoNextUser(): void {
    const id: number = +this.activatedRoute.snapshot.params.id;
    this.router.navigateByUrl(`users-details/${id + 1}`);
  }
}

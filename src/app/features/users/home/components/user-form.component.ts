import {
  AfterViewInit, ApplicationRef, ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { User } from '../../../../model/user';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h1 [style.color]="color">My form</h1>
    <form #f="ngForm" (submit)="saveHandler(f)">
      <input type="text" #inputName [ngModel]="activeUser?.name" name="name" required>
      <input type="text" [ngModel]="activeUser?.email" name="email">
      <button type="submit" [disabled]="f.invalid">
        {{activeUser?.id ? 'EDIT' : 'ADD'}}
      </button>
      <button type="button" (click)="resetHandler(f)">CLEAR</button>
    </form>
  `,
})
export class UserFormComponent implements OnChanges, AfterViewInit {
  @ViewChild('f', { static: true }) form: NgForm;
  @ViewChild('inputName') inputName: ElementRef;
  @Input() activeUser: User;
  @Input() color: string;
  @Output() save: EventEmitter<User> = new EventEmitter<User>();
  @Output() reset: EventEmitter<void> = new EventEmitter<void>();

  constructor(private app: ApplicationRef) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.activeUser.firstChange) {
      // this.init();
    }
    if (changes.activeUser && !changes.activeUser.currentValue?.id) {
      this.form.reset();
    }

    if (changes.color) {
      // do something
    }
  }

  ngAfterViewInit(): void {
    this.inputName.nativeElement.focus();
  }
  saveHandler(f: NgForm): void {
    this.save.emit(f.value);
  }

  resetHandler(f: NgForm): void {
    this.reset.emit()
    f.reset();
  }

  clean(): void {
    console.log('form reset')
  }
}

export interface Auth {
  accessToken: string;
  displayName: string;
  role: string;
}

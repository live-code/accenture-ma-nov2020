import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  template: `

    <button routerLink="login" routerLinkActive="bg-dark">login</button>
    <button routerLink="home" routerLinkActive="bg-dark">home</button>
    <button routerLink="demo" routerLinkActive="bg-dark">demo components</button>
    <button *ngIf="authService.isLogged()" routerLink="users" routerLinkActive="bg-dark">users</button>
    <button *appIfSignIn (click)="logoutHandler()">Logout</button>
    
    {{authService.displayName}}
  `,
})
export class NavbarComponent{

  constructor(
    public authService: AuthService,
    private router: Router
  ) { }

  logoutHandler(): void {
    this.authService.logout();
    this.router.navigateByUrl('login')
  }
}

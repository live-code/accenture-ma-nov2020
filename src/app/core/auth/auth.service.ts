import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Auth } from '../../model/auth';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { catchError, share } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthService {
  auth: Auth;
  err: boolean;
  logged: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {
    this.auth = JSON.parse(localStorage.getItem('auth')) || null;
    if (this.auth) {
      this.logged.next(true);
    }
  }


  login(username: string, password: string): Observable<Auth> {
    this.err = false;
    const obs = this.http.get<Auth>('http://localhost:3000/login')
      .pipe(
        share()
      );
    obs.subscribe(
        res => {
          localStorage.setItem('auth', JSON.stringify(res))
          this.auth = res;
          this.logged.next(true);
        },
        () => {
          this.err = true;
          this.logged.next(false);
        }
      );
    return obs;
  }

  logout(): void {
    this.auth = null;
    this.logged.next(false);
    localStorage.removeItem('auth')
  }

  isLogged(): boolean {
    return !!this.auth;
  }

  get token(): string {
    return this.auth?.accessToken;
  }

  get role(): string {
    return this.auth?.role;
  }
  get displayName(): string {
    return this.auth?.displayName;
  }
}

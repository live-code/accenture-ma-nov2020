import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: ` 
    <div>
      {{title}}
      <button (click)="buttonClick.emit()">{{labelButton}}</button>
    </div>
  `
})
export class HelloComponent {
  @Input() title = 'ciao';
  @Input() labelButton = 'CLICK ME';
  @Output() buttonClick = new EventEmitter();
}

import { Injectable } from '@angular/core';
import { User } from '../../../../model/user';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class UsersService {
  users: User[];
  activeUser: User = {} as User;

  constructor(private http: HttpClient) {}

  init(): void {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => this.users = res);
  }

  deleteHandler(id: number): void {
    this.http.delete('http://localhost:3000/users/' + id)
      .subscribe(
        () => {
          this.users = this.users.filter(u => u.id !==  id);
          if (this.activeUser?.id === id) {
            this.resetHandler();
          }
        },
        err => {
          window.alert('errore')
        }
      );
  }

  saveHandler(user: User): void {
    if (this.activeUser?.id) {
      this.editHandler(user);
    } else {
      this.addHandler(user);
    }
  }

  editHandler(user: User): void {
    this.http.patch<User>('http://localhost:3000/users/' + this.activeUser.id, user)
      .subscribe(() => {
        const users =  JSON.parse(JSON.stringify(this.users));
        this.users = users.map(u => {
          return u.id === this.activeUser?.id ? {...u, ...user} :  u;
        });
      });
  }

  addHandler(user: User): void {
    this.http.post<User>('http://localhost:3000/users', user)
      .subscribe(res => {
        this.users = [...this.users, res];
        this.resetHandler();
      });
  }

  setActiveHandler(user: User): void {
    this.activeUser = user;
  }

  resetHandler(): void {
    this.activeUser = {} as User;
  }
}

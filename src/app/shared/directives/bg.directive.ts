import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appBg]'
})
export class BgDirective {
  @Input() appBg: 'success' | 'error';
  @HostBinding('style.color') get color(): string {
    return this.appBg;
  }

}


// getElementById('.pippo').style.color = 'white
// getElementById('.pippo').innerHTML =

import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';

@Directive({
  selector: '[appIfSignIn]'
})
export class IfSignInDirective {

  constructor(
    view: ViewContainerRef,
    template: TemplateRef<any>,
    authService: AuthService
  ) {
    authService.logged
      .subscribe(isLogged => {
        if (isLogged) {
          view.createEmbeddedView(template)
        } else {
          view.clear();
        }
      })
  }

}

export interface Country {
  id: number;
  label: string;
  description: string;
  cities: City[];
}

export interface City {
  id: number;
  name: string;
}

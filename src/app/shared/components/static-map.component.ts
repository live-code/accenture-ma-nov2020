import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-static-map',
  template: `
    <img [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + city + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
  `,
})
export class StaticMapComponent {
  @Input() city: string;
}

import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appUrl]'
})
export class UrlDirective {
  @Input() appUrl: string;

  @HostBinding('style.cursor') get border(): string {
    return 'pointer'
  }

  @HostListener('click')
  clickHandler(): void {
    window.open(this.appUrl)
  }

  constructor() { }

}

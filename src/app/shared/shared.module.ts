import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabbarComponent } from './components/tabbar.component';
import { HelloComponent } from './components/hello.component';
import { PanelComponent } from './components/panel.component';
import { StaticMapComponent } from './components/static-map.component';
import { BgDirective } from './directives/bg.directive';
import { UrlDirective } from './directives/url.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { IfSignInDirective } from './directives/if-sign-in.directive';

@NgModule({
  declarations: [
    HelloComponent,
    TabbarComponent,
    StaticMapComponent,
    PanelComponent,
    BgDirective,
    UrlDirective,
    IfLoggedDirective,
    IfSignInDirective,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    TabbarComponent,
    PanelComponent,
    StaticMapComponent,
    HelloComponent,
    BgDirective,
    UrlDirective,
    IfLoggedDirective,
    IfSignInDirective
  ]
})
export class SharedModule { }

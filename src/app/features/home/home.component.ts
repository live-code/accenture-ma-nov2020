import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  template: `
    <app-panel></app-panel>
    
    <form [formGroup]="form" (submit)="save()">
      <input type="text" formControlName="username">
      
      
      <div formGroupName="anagrafica" style="padding: 20px; border: 2px solid grey" 
           [style.border-color]="form.get('anagrafica').valid ? 'green' : 'red'">
        <input type="text" formControlName="name">
        <input type="text" formControlName="surname">
      </div>
      <button [disabled]="form.invalid" type="submit">GO</button>
    </form>
  `,
  styles: [
  ]
})
export class HomeComponent {
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      username: ['guest', Validators.compose([Validators.required, alphaNumeric])],
      anagrafica: fb.group({
        name: ['', Validators.required],
        surname: ''
      })
    });

    const res = { 
      username: 'pippo',
      anagrafica: {
        name: '1', surname: '2'
      }
    };

    this.form.setValue(res)
  }

  save() {
    console.log(this.form.value)
  }
}

const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;

export function alphaNumeric(c: FormControl): ValidationErrors {
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return { alphanumeric: true };
  }
}

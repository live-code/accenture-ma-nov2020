import { Component } from '@angular/core';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-users-home',
  template: `
    <hr>
    <app-user-form 
      [activeUser]="userService.activeUser" 
      color="red" 
      (save)="userService.saveHandler($event)" 
      (reset)="userService.resetHandler()"
    ></app-user-form>
    
    <app-user-list 
      [users]="userService.users" 
      [activeUser]="userService.activeUser" 
      (setActiveUser)="userService.setActiveHandler($event)" 
      (deleteUser)="userService.deleteHandler($event)"
    ></app-user-list>
  `,
})
export class UsersHomeComponent {
  constructor(public userService: UsersService) {
    userService.init();
  }
}


import { Directive, HostBinding } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';

@Directive({
  selector: '[appIfLogged]'
})
export class IfLoggedDirective {

  constructor(private authService: AuthService) {
  }

  @HostBinding('style.display') get display(): string {
    return this.authService.isLogged() ? null : 'none';
  }

}

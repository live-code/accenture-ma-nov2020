import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { UsersGuideComponent } from './guide/users-guide.component';
import { UsersGroupsComponent } from './groups/users-groups.component';
import { UsersHomeComponent } from './home/users-home.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: UsersComponent,
        children: [
          { path: 'home', component: UsersHomeComponent },
          { path: 'guide', component: UsersGuideComponent},
          { path: 'groups', component: UsersGroupsComponent},
          { path: '', redirectTo: 'home'}
        ]
      },
    ]),
  ],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
